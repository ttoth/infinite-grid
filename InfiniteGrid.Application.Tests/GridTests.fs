﻿namespace InfiniteGrid.Application.Tests

open NUnit.Framework
open InfiniteGrid.Application
open InfiniteGrid.Domain.Entities

[<TestFixture>]
module GridTests =
    let mutable sut: Grid = Grid()

    [<SetUp>]
    let SetUp () =
        sut <- Grid()

    [<Test>]
    let ``Grid is initially empty`` ()=
        CollectionAssert.IsEmpty sut.Blacks


    [<Test>]
    let ``Flipping a white cell becames black`` ()=
        Assert.AreEqual(Black, sut.FlipAt {X = 1; Y = 2})
        Assert.AreEqual(Black, sut.ColorAt {X = 1; Y = 2})
        CollectionAssert.AreEqual([{X = 1; Y = 2}], sut.Blacks)

    [<Test>]
    let ``Flipping a black cell becames white`` ()=
        sut.FlipAt {X = 1; Y = 2} |> ignore

        Assert.AreEqual(White, sut.FlipAt {X = 1; Y = 2})
        Assert.AreEqual(White, sut.ColorAt {X = 1; Y = 2})
        CollectionAssert.IsEmpty sut.Blacks