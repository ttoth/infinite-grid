﻿namespace InfiniteGrid.Application.Tests

open NUnit.Framework
open InfiniteGrid.Application
open InfiniteGrid.Domain.Entities

[<TestFixture>]
module RobotTests =
    let mutable sut: Robot = Robot({X = 0; Y = 0}, Right)

    [<SetUp>]
    let SetUp () =
        sut <- Robot({X = 0; Y = 0}, Right)

    [<Test>]
    let ``Move right 2 cells`` ()=
        sut.Move 2
        Assert.AreEqual(Right, sut.Orientation)
        Assert.AreEqual({X = 2; Y = 0}, sut.Position)


    [<Test>]
    let ``Move up 2 cells`` ()=
        sut.Orientation <- Up
        sut.Move 2

        Assert.AreEqual(Up, sut.Orientation)
        Assert.AreEqual({X = 0; Y = 2}, sut.Position)

    [<Test>]
    let ``Turn clockwise`` ()=
        sut.Turn CW

        Assert.AreEqual(Down, sut.Orientation)
        Assert.AreEqual({X = 0; Y = 0}, sut.Position)