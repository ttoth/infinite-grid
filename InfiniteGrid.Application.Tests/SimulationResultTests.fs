namespace InfiniteGrid.Application.Tests

open InfiniteGrid.Domain.Entities
open InfiniteGrid.Application.Responses
open NUnit.Framework

[<TestFixture>]
module ``When mapping points to Success result`` =

    [<Test>]
    let ``Empty sequence returns empty array`` () =
        let result = Success Seq.empty<Point>
        match result with
        | Success success -> CollectionAssert.IsEmpty success
        | Error error -> Assert.Fail error

    [<Test>]
    let ``Create with 1 point returns single black cell`` () =
        let result = Success [{X = 2; Y = 3}]
        match result with
        | Success success -> CollectionAssert.AreEqual([|"#"|], success)
        | Error error -> Assert.Fail error

    [<Test>]
    let ``Create with 3 point returns 3 black cells`` () =
        let result = Success [
            {X = 0; Y = 0}
            {X = 1; Y = 1}
            {X = 2; Y = 3}
        ]

        let expected = [|
            "  #"
            "   "
            " # "
            "#  "
        |]

        match result with
        | Success success -> CollectionAssert.AreEqual(expected, success)
        | Error error -> Assert.Fail error
