﻿namespace InfiniteGrid.Application

open InfiniteGrid.Domain.Entities
open System.Collections.Generic

type Grid() = 
    let _blacks: HashSet<Point> = new HashSet<Point>()

    member this.FlipAt point =
        if _blacks.Add point
        then Black
        else
            _blacks.Remove point |> ignore
            White

    member this.ColorAt point =
        if _blacks.Contains point
        then Black
        else White

    member this.Blacks =
        seq _blacks