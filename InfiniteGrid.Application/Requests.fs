﻿module InfiniteGrid.Application.Requests

[<CLIMutable>]
type SimulationResult = { NumberOfSteps : int }