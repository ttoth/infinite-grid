﻿module InfiniteGrid.Application.Responses
open InfiniteGrid.Domain.Entities

type SimulationResult = 
    | Error of string
    | Success of string[]

let Success(blacks: seq<Point>) =
    if Seq.isEmpty blacks then Success Array.empty
    else

    let get fn p1 p2 = { X = fn p1.X p2.X ; Y = fn p1.Y p2.Y }

    let first = Seq.head blacks
    let mutable bottomLeft = first
    let mutable topRight = first

    for point in Seq.tail blacks do
        bottomLeft <- get min bottomLeft point
        topRight <- get max topRight point

    topRight <- get (-) topRight bottomLeft

    let result = seq { for i in 0 .. topRight.Y -> Array.create (topRight.X+1) ' '} |> Seq.toArray

    for point in blacks do
        let pos = get (-) point bottomLeft
        result.[topRight.Y - pos.Y].[pos.X] <- '#'

    Success <| (result
        |> Array.map (fun c -> new string(c)))