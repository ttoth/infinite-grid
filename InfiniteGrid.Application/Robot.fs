﻿namespace InfiniteGrid.Application

open InfiniteGrid.Domain.Entities
open InfiniteGrid.Domain.Direction

type Robot =
    val mutable Position : Point
    val mutable Orientation : Direction

    new (position, orientation) =
        { Position = position
          Orientation = orientation }

    member this.Move step =
        let dx, dy =
            match this.Orientation with
            | Left  -> (-1,  0)
            | Right -> ( 1,  0)
            | Up    -> ( 0,  1)
            | Down  -> ( 0, -1)

        let change value d = value + d * step

        this.Position <- 
            { X = change this.Position.X dx ;
              Y = change this.Position.Y dy }

    member this.Turn rotation =
        this.Orientation <- this.Orientation.Rotate rotation

