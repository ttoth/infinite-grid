﻿namespace InfiniteGrid.Application

open InfiniteGrid.Domain.Entities
open InfiniteGrid.Application.Responses

type Simulation() =
    let _robot: Robot = Robot({ X = 0; Y = 0 }, Right)
    let _grid: Grid = Grid()

    let Step() =
        let getRotation color = match color with | Black -> CCW | White ->CW

        _robot.Turn(_grid.ColorAt _robot.Position |> getRotation)

        _grid.FlipAt _robot.Position |> ignore

        _robot.Move(1)
        ()

    member this.Run numberOfSteps =
        if numberOfSteps < 0
        then Error "Number of steps cannot be negative!"
        else

        for i = 1 to numberOfSteps do
            Step()

        Success _grid.Blacks
