namespace InfiniteGrid.Domain.Tests

open NUnit.Framework
open InfiniteGrid.Domain.Entities
open InfiniteGrid.Domain.Direction

[<TestFixture>]
module DirectionTests =
    let rotateCWCases = 
        [
            TestCaseData(Left).Returns(Up)
            TestCaseData(Right).Returns(Down)
            TestCaseData(Up).Returns(Right)
            TestCaseData(Down).Returns(Left)
        ]

    let rotateCCWCases = 
        [
            TestCaseData(Left).Returns(Down)
            TestCaseData(Right).Returns(Up)
            TestCaseData(Up).Returns(Left)
            TestCaseData(Down).Returns(Right)
        ]

    [<Test>]
    [<TestCaseSource("rotateCWCases")>]
    let ``Rotating clockwise`` (direction: Direction) =
        direction.Rotate CW

    [<Test>]
    [<TestCaseSource("rotateCCWCases")>]
    let ``Rotating counter-clockwise`` (direction: Direction) =
        direction.Rotate CCW
