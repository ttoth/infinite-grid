﻿module InfiniteGrid.Domain.Direction 

open InfiniteGrid.Domain.Entities

let Rotate direction rotation = 
    let RotateCW =
        match direction with
        | Left  -> Up
        | Up    -> Right
        | Right -> Down
        | Down  -> Left

    let RotateCCW =
        match direction with
        | Left  -> Down
        | Down  -> Right
        | Right -> Up
        | Up    -> Left

    match rotation with
        | CW -> RotateCW
        | CCW -> RotateCCW

open System.Runtime.CompilerServices
[<Extension>]
type DirectionExtension() =
    [<Extension>]
    static member inline Rotate(direction, rotation) = Rotate direction rotation

