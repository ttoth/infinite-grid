﻿module InfiniteGrid.Domain.Entities

[<Struct>]
type public Direction = Left | Right | Up | Down

[<Struct>]
type Rotation =  CW | CCW

[<Struct>]
type Color = Black | White

[<Struct>]
type Point = 
    { X: int
      Y: int }
