﻿namespace InfiniteGrid.Web

open Microsoft.AspNetCore.Mvc
open InfiniteGrid.Application
open InfiniteGrid.Application.Responses

[<Route("api/[controller]")>]
type SimulationController =
    inherit ControllerBase

    val _simulation : Simulation
    new(simulation: Simulation) = { inherit ControllerBase(); _simulation = simulation }
 
    [<HttpPut>]
    member this.GetSimulationResult([<FromBody>]request: Requests.SimulationResult) : IActionResult=
        let result = this._simulation.Run request.NumberOfSteps

        match result with
            | Success success -> this.Ok(success) :> IActionResult
            | Error error -> this.BadRequest(error) :> IActionResult
